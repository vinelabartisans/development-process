module.exports = function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json')
    });

    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.config('coffee', {
        compile:{
            expand: true,
            cwd: 'app/scripts',
            src: ['**/*.coffee'],
            dest: 'app/scripts',
            ext: '.js'
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.config('less', {
        development:{
            options:{
                paths: ["app/styles"]
            },
            src: ['<%= concat.less.dest %>'],
            dest: 'app/styles/main.css'
        } 
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.config('concat', {
        options:{
            seperator: ','
        },
        scripts: {
            src: ['app/scripts/App.js', 'app/scripts/main/**/*.js'],
            dest: 'dist/build.js'
        },
        less:{
            src: ['app/styles/less/common/variables.less', 'app/styles/less/**/*.less'],
            dest: 'app/styles/main.less'
        }
        
    });

    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.config('ngAnnotate', {
        app: {
            src: ['<%= concat.scripts.dest %>'],
            dest: 'dist/build.annotated.js'
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.config('uglify', {
        my_target: {
            files:{
                'dist/build.min.js': ['<%= ngAnnotate.app.dest %>']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.config('watch', {
        options:{
            livereload: true
        },
        scripts:{
            files: ['app/scripts/**/*.coffee'],
            tasks: ['coffee', 'concat']
        },
        css:{
            files: ['app/styles/less/**/*.less'],
            tasks: ['concat', 'less']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.config('connect', {
        server:{
            options:{
                port: 9090,
                hostname: '0.0.0.0'
            }
        }
    });

    grunt.loadNpmTasks('grunt-processhtml');
    grunt.config('processhtml',{
        html:{
            files:{
                'dist/index.html': ['index.html']
            }
        }
    });

    

    grunt.registerTask('dev', ['coffee', 'concat', 'less:development', 'connect', 'watch']);
    grunt.registerTask('default', ['coffee' ,'concat', 'ngAnnotate', 'uglify']);
};
